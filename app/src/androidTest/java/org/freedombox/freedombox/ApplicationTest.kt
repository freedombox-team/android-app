// SPDX-License-Identifier: GPL-3.0-or-later

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ApplicationTest {

    @Test
    fun useAppContext() {
        val appContext = ApplicationProvider.getApplicationContext<Application>()
        assertNotNull(appContext) // Simple assertion to check if context is not null
    }
}