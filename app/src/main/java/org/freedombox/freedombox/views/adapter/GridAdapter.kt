// SPDX-License-Identifier: GPL-3.0-or-later

package org.freedombox.freedombox.views.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import org.freedombox.freedombox.databinding.AppContainerBinding
import org.freedombox.freedombox.models.Shortcut
import org.freedombox.freedombox.utils.ImageRenderer
import org.freedombox.freedombox.utils.network.launchApp
import org.freedombox.freedombox.utils.network.urlJoin
import java.util.Locale

class GridAdapter(val context: Context, val imageRenderer: ImageRenderer, val baseUrl: String) : BaseAdapter() {

    private var items = listOf<Shortcut>()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val binding = if (convertView == null) {
            AppContainerBinding.inflate(LayoutInflater.from(context), parent, false)
        } else {
            AppContainerBinding.bind(convertView)
        }

        val shortcut = items[position]

        binding.appName.text = shortcut.name
        binding.appDescription.text = shortcut.shortDescription

        /* TODO Implement fallback to png on svg image render failure automatically
         * This will ensure that manually installed applications will also be rendered correctly.
         */

        // Apps whose SVG files cannot be rendered by Android
        val brokenSVGs = listOf(
            "bepasty", "deluge", "mumble", "minetest",
            "transmission", "tiny tiny rss", "zoph"
        )

        if (shortcut.name.lowercase(Locale.getDefault()) in brokenSVGs)
            imageRenderer.loadImageFromURL(
                Uri.parse(urlJoin(baseUrl, shortcut.iconUrl)),
                binding.appIcon
            )
        else
            imageRenderer.loadSvgImageFromURL(
                    Uri.parse(urlJoin(baseUrl, shortcut.iconUrl.replace(".png", ".svg"))),
                    binding.appIcon
            )

        binding.appIcon.setOnClickListener { launchApp(shortcut, context, baseUrl) }
        return binding.root
    }

    override fun getItem(position: Int): Shortcut = items[position]

    override fun getItemId(position: Int) = items[position].hashCode().toLong()

    override fun getCount() = items.size

    @Suppress("SENSELESS_COMPARISON")
    fun setData(shortcuts: List<Shortcut>) {
        items = shortcuts.filter{ it.clients != null}

        notifyDataSetChanged()
    }
}
