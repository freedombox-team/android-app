// SPDX-License-Identifier: GPL-3.0-or-later

package org.freedombox.freedombox.views.fragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import org.freedombox.freedombox.R
import org.freedombox.freedombox.databinding.FragmentSetupBinding
import org.freedombox.freedombox.utils.network.wrapHttps
import org.freedombox.freedombox.utils.storage.getConfiguredBoxesMap
import org.freedombox.freedombox.utils.storage.getSharedPreference
import org.freedombox.freedombox.utils.storage.putSharedPreference
import org.freedombox.freedombox.utils.view.getEnteredText
import org.freedombox.freedombox.utils.view.getSwitchStatus
import org.freedombox.freedombox.views.activities.DiscoveryActivity
import org.freedombox.freedombox.views.model.ConfigModel
import javax.inject.Inject

class SetupFragment : Fragment() {

    @Inject lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_setup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        val binding = FragmentSetupBinding.bind(view)

        // Deprecation warning can't be fixed without breaking compatibility with Android 4.4 KitKat
        val currentBox = arguments?.getParcelable<ConfigModel>(getString(R.string.current_box))

        currentBox?.apply {
            binding.boxName.setText(boxName)
            binding.discoveredUrl.setText(domain)
            binding.defaultStatus.isChecked = isDefault()
        }

        binding.saveConfig.setOnClickListener {
            storeFreedomBoxConfiguration(binding)
            requireActivity().finish()
        }

        val configuredBoxesJSON = getSharedPreference(sharedPreferences,
                getString(R.string.saved_boxes))

        val boxes = getConfiguredBoxesMap(configuredBoxesJSON)

        // Show delete button only if the box has already been saved
        if (boxes != null && currentBox != null) {
            if (boxes.containsKey(currentBox.boxName)) {
                binding.deleteConfig.visibility = View.VISIBLE
            }
        }

        binding.deleteConfig.setOnClickListener{
            deleteFreedomBoxConfiguration(binding)
            startActivity(Intent(activity, DiscoveryActivity::class.java))
        }
    }

    private fun deleteFreedomBoxConfiguration(binding: FragmentSetupBinding) {
        val configuredBoxesJSON = getSharedPreference(sharedPreferences,
                getString(R.string.saved_boxes))

        val boxes = getConfiguredBoxesMap(configuredBoxesJSON)
        boxes?.let {
            putSharedPreference(sharedPreferences, getString(R.string.saved_boxes),
                    it.minus(getEnteredText(binding.boxName)))
        }
    }

    private fun storeFreedomBoxConfiguration(binding: FragmentSetupBinding) {
        val configuredBoxesJSON = getSharedPreference(sharedPreferences,
            getString(R.string.saved_boxes))

        val savedBoxes = getConfiguredBoxesMap(configuredBoxesJSON) ?: mapOf()

        val updatedBoxes = if(savedBoxes.isNotEmpty() && getSwitchStatus(binding.defaultStatus)) {
            val previousDefault = savedBoxes.filterValues { it.isDefault() }.entries.firstOrNull()
            previousDefault?.let {
                savedBoxes.plus(Pair(previousDefault.key, previousDefault.value.copy(default = false)))
            } ?: savedBoxes
        } else savedBoxes

        val configModel = ConfigModel(
                getEnteredText(binding.boxName),
                wrapHttps(getEnteredText(binding.discoveredUrl)),
                // Make the first configured FreedomBox the default by default
                getSwitchStatus(binding.defaultStatus) or savedBoxes.isEmpty())

        val configuredBoxMap = updatedBoxes.plus(Pair(configModel.boxName, configModel))

        putSharedPreference(sharedPreferences,
            getString(R.string.saved_boxes),
            configuredBoxMap)
    }

    companion object {
        fun new(args: Bundle): SetupFragment {
            val fragment = SetupFragment()
            fragment.arguments = args

            return fragment
        }
    }
}
