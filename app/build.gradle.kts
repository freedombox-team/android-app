// SPDX-License-Identifier: GPL-3.0-or-later

object Versions {
    const val DAGGER = "2.52"
    const val SUPPORTLIB = "1.0.0"
}

plugins {
    id("com.android.application")
    id("com.mikepenz.aboutlibraries.plugin")
    id("com.google.devtools.ksp")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdk = 34

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    buildFeatures {
        viewBinding = true
    }

    defaultConfig {
        applicationId = "org.freedombox.freedombox"
        minSdk = 19
        targetSdk = 34
        maxSdk = 34
        versionCode = 9
        versionName = "0.7"
        vectorDrawables.useSupportLibrary = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }

    namespace = "org.freedombox.freedombox"

    lint {
        abortOnError = false
    }

    buildFeatures {
        buildConfig = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation("androidx.preference:preference-ktx:1.2.1")

    implementation("com.google.android.material:material:${Versions.SUPPORTLIB}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:2.0.0")
    implementation("com.android.volley:volley:1.2.1")
    implementation("com.google.code.gson:gson:2.8.5")

    // About Libraries
    implementation("com.mikepenz:aboutlibraries:6.1.1")

    implementation("androidx.appcompat:appcompat:${Versions.SUPPORTLIB}")
    implementation("androidx.cardview:cardview:${Versions.SUPPORTLIB}")
    implementation("androidx.recyclerview:recyclerview:${Versions.SUPPORTLIB}")
    implementation("androidx.annotation:annotation:${Versions.SUPPORTLIB}}")

    // Dagger
    implementation("com.google.dagger:dagger:${Versions.DAGGER}")
    implementation("androidx.test:core-ktx:1.6.1")
    implementation("androidx.test.ext:junit-ktx:1.2.1")
    kapt("com.google.dagger:dagger-compiler:${Versions.DAGGER}")

    // Glide
    implementation("com.github.bumptech.glide:glide:3.8.0")
    implementation("androidx.legacy:legacy-support-v4:${Versions.SUPPORTLIB}")
    implementation("com.caverock:androidsvg:1.2.1")

    // Test
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.robolectric:robolectric:4.11.1")
    testImplementation("androidx.test:core:1.6.1")
    testImplementation("androidx.test.ext:junit:1.2.1")
    testImplementation("androidx.test.espresso:espresso-core:3.6.1")
}
