// SPDX-License-Identifier: GPL-3.0-or-later

plugins {
    id("com.android.application") version "8.3.2" apply false
    id("org.jetbrains.kotlin.android") version "2.0.0" apply false
    id("com.google.devtools.ksp") version "2.0.0-1.0.21" apply false
    id("com.mikepenz.aboutlibraries.plugin") version "11.1.3" apply false
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}